# ExplorysConvertor
Installation
Lancez le script Install.sh
sh ./Install.sh

Après l’installation, deux dossier sont crées :
- ExplorysConvertor (Dossier du convertisseur)
- free-cad-code (Dossier FreeCAD)
Dans le dossier ExplorysConvertor/Convertor : 
- convertor.jar (Le convertisseur)
- filter_linux.mlx (Le filtre exécuté si l’OS est Linux)
- filter_windows.mlx (Le filtre exécuté si l’OS est Windows)
- Hibernate.cfg.xml (La configuration hibernate pour l’accès a la base de donnée)
- rep.txt (la configuration de convertisseur)
rep.txt
Elle contient les préférences pour le convertor :
6 valeurs sont enregistrées :
- stl: Format de sortie
- (double) : Tessellation (Valeur entre  0,000001 à 1)
- (int) : LOD – Level of Details
- (string) : Emplacement de FreeCAD
- (string) : Emplacement de MeshLabServer
- (boolean) : Generer des meshs par FreeCAD
- (boolean) : Optimiser des meshs par MeshLabServer
- (boolean) : Envoyer les données a la base de donnée
- (boolean) : (False) Exécuter en ligne de commande ou (True) lancer l’interface graphique
Liste des instructions du convertor
Commande de base (Valable pour toute la suite):
java -Xmx8g -Xms1g -jar convertor.jar
[param value] ==> Paramètre optionel

Démarrer l’interface graphique du convertor
-gui start

Commande valable pour la base de donées :
Affecter un parent B au noeud A dans la base de données (Connexion a la base est obligatoire)
-setParent nodeA_UUID -parent nodeB_UUID [-axis “0,0,0”] [-angle 0] [“-isRoot” true or false default:false]

Importer un dossier des stp
-id inputURL  [-of outputURL default:same directory] [-e “stl” ou “obj” default:stl] [-sub true or false default:true] [-t from 0,000001 to 1 default:1]

Importer un fichier stp
-if inputURL  [-od outputURL default:same directory] [-e “stl” ou “obj” default:stl] [-t from 0,000001 to 1 default:1]


Importer un dossier XML (Connexion a la base est obligatoire)
-ixmld inputDirectoryURL 

Importer un fichier XML (Connexion a la base est obligatoire)
-ixmlf inputFileURL


Liste des instructions du  player
function :: addNode (UUID node , int LOD)
Add a node with a specific LOD to the scene
function ::String getPath (UUID node , String separator)
Get the complete URL to the specific node using the separator
function ::UUID getNodeId (String path , String separator)
Get the UUID of the specific node returned by fetching the path with the separator.
function ::Group getNode ( UUID node , int LOD)
Get the Group “Assembly” of the node with the LOD specified
