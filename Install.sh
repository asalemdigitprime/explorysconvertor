#!/bin/bash
clear
sudo apt install -y git 
sudo apt-get install -y cmake
clear
echo "Getting Explorys Convertor"
REPOSRC="https://asalemdigitprime@bitbucket.org/asalemdigitprime/explorysconvertor.git"
LOCALREPO="ExplorysConvertor"
LOCALREPO_VC_DIR=$LOCALREPO/.git
if [ ! -d $LOCALREPO/.git ]
then
git clone $REPOSRC $LOCALREPO
else
cd $LOCALREPO
git pull $REPOSRC
cd ..
fi
clear
echo "Getting FreeCAD ..."
REPOSRC="https://github.com/akramsalem/freecad.git"
LOCALREPO="free-cad-code"
LOCALREPO_VC_DIR=$LOCALREPO/.git
if [ ! -d $LOCALREPO/.git ]
then
git clone $REPOSRC $LOCALREPO
else
cd $LOCALREPO
git pull $REPOSRC
cd ..
fi
clear
sudo chmod -R 777 */
echo "Installing FreeCad dependencies ..."
sudo apt install -y build-essential cmake python python-matplotlib libtool libcoin80-dev libsoqt4-dev libxerces-c-dev libboost-dev libboost-filesystem-dev libboost-regex-dev libboost-program-options-dev libboost-signals-dev libboost-thread-dev libboost-python-dev libqt4-dev libqt4-opengl-dev qt4-dev-tools python-dev python-pyside pyside-tools libeigen3-dev libqtwebkit-dev libshiboken-dev libpyside-dev libode-dev swig libzipios++-dev libfreetype6-dev liboce-foundation-dev liboce-modeling-dev liboce-ocaf-dev liboce-visualization-dev liboce-ocaf-lite-dev libsimage-dev checkinstall python-pivy python-qt4 doxygen libspnav-dev oce-draw liboce-foundation-dev liboce-modeling-dev liboce-ocaf-dev liboce-ocaf-lite-dev liboce-visualization-dev libmedc-dev libvtk6-dev libproj-dev 
clear
sudo apt-get -y update
echo "Regenerating FreeCAD..."
cd free-cad-code
sudo cmake -DFREECAD_USE_EXTERNAL_PIVY=1 -DCMAKE_BUILD_TYPE=Release .
NB_CORES=$(grep -c '^processor' /proc/cpuinfo)
export MAKEFLAGS="-j$((NB_CORES+1)) -l${NB_CORES}"
sudo make
cd ..
FREECAD_PATH=`readlink -f free-cad-code/bin/FreeCAD`
clear
echo "Installing MeshLab ..."
sudo add-apt-repository ppa:zarquon42/meshlab
sudo apt-get update
sudo apt-get install -y meshlab
clear
MESHLAB_PATH="/usr/bin/meshlabserver"
echo "Creating Rep File ..."
touch ExplorysConvertor/Convertor/rep.txt
sudo chmod 777 ExplorysConvertor/Convertor/rep.txt
echo "stl;1;7;$FREECAD_PATH;$MESHLAB_PATH;true;true;true;true" > ExplorysConvertor/Convertor/rep.txt
clear
echo "Installing ORACLE / JRE ..."
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install -y oracle-java8-installer
clear

